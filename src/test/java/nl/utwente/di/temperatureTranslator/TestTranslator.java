package nl.utwente.di.temperatureTranslator;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Test class for Quoter.
 */
public class TestTranslator {
    @Test
    public void testBook1 ( ) throws Exception {
        Translator quoter = new Translator();
        double price = quoter.getFahrenheit("1");
        Assertions.assertEquals(33.8, price,0.0, "Celcius in Fahrenheit");
    }
}