package nl.utwente.di.temperatureTranslator;

import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

/** Example of a Servlet that gets a temperature in celcius and returns the fahrenheit value
 */

public class TemperatureTranslator extends HttpServlet {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Translator quoter;

    public void init() throws ServletException {
        quoter = new Translator();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String title = "Temperature Translator";

        // Done with string concatenation only for the demo
        // Not expected to be done like this in the project
        out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Temperature: " +
                request.getParameter("temperature") + "\n" +
                "  <P>Price: " +
                quoter.getFahrenheit(request.getParameter("temperature")) +
                "</BODY></HTML>");
    }
}
